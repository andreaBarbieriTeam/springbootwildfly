package com.example.springbootwildfly.helloWorld.soap;

import com.example.springbootwildfly.SpringbootwildflyApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringbootwildflyApplication.class)
@WebAppConfiguration
public class HelloWorldWSSOAPTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void helloWorld() throws Exception {

        /*final QName SERVICE_NAME = new QName("http://localhost:7001/springbootwildfly/helloWorldWSSOAP/", "helloWorldWSSOAP");
        URL wsdlURL = HelloWorldWSSOAP_Service.WSDL_LOCATION;
        HelloWorldWSSOAP_Service ss = new HelloWorldWSSOAP_Service(wsdlURL, SERVICE_NAME);
        HelloWorldWSSOAP port = ss.getHelloWorldWSSOAPSOAP();
        int _helloWorld_codeRequest = 0;
        java.lang.String _helloWorld_descriptionRequest = "";
        javax.xml.datatype.XMLGregorianCalendar _helloWorld_dateElementRequest = null;
        javax.xml.datatype.XMLGregorianCalendar _helloWorld_dateTimeElementRequest = null;
        javax.xml.datatype.XMLGregorianCalendar _helloWorld_timeElementRequest = null;
        float _helloWorld_floatElementRequest = 0.0f;
        double _helloWorld_doubleElementRequest = 0.0;
        javax.xml.ws.Holder<java.lang.Integer> _helloWorld_codeResponse = new javax.xml.ws.Holder<java.lang.Integer>();
        javax.xml.ws.Holder<java.lang.String> _helloWorld_descriptionResponse = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar> _helloWorld_dateElementResponse = new javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar>();
        javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar> _helloWorld_dateTimeElementResponse = new javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar>();
        javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar> _helloWorld_timeElementResponse = new javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar>();
        javax.xml.ws.Holder<java.lang.Double> _helloWorld_doubleElementResponse = new javax.xml.ws.Holder<java.lang.Double>();
        try {
            port.helloWorld(_helloWorld_codeRequest, _helloWorld_descriptionRequest, _helloWorld_dateElementRequest,
                    _helloWorld_dateTimeElementRequest, _helloWorld_timeElementRequest, _helloWorld_floatElementRequest,
                    _helloWorld_doubleElementRequest, _helloWorld_codeResponse, _helloWorld_descriptionResponse,
                    _helloWorld_dateElementResponse, _helloWorld_dateTimeElementResponse, _helloWorld_timeElementResponse,
                    _helloWorld_doubleElementResponse);

            System.out.println("helloWorld._helloWorld_codeResponse=" + _helloWorld_codeResponse.value);
            System.out.println("helloWorld._helloWorld_descriptionResponse=" + _helloWorld_descriptionResponse.value);
            System.out.println("helloWorld._helloWorld_dateElementResponse=" + _helloWorld_dateElementResponse.value);
            System.out.println("helloWorld._helloWorld_dateTimeElementResponse=" + _helloWorld_dateTimeElementResponse.value);
            System.out.println("helloWorld._helloWorld_timeElementResponse=" + _helloWorld_timeElementResponse.value);
            System.out.println("helloWorld._helloWorld_doubleElementResponse=" + _helloWorld_doubleElementResponse.value);
        } catch (HelloWorldFault_Exception e) {
            System.out.println("Expected exception: HelloWorldFault has occurred.");
            System.out.println(e.toString());
        }*/
    }

}