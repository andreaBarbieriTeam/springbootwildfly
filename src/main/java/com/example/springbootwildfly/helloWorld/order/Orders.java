package com.example.springbootwildfly.helloWorld.order;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Orders {
    private long orderId;
    private Timestamp insertDate;
    private Timestamp lastUpdateDate;
    private Long optlock;

    @Id
    @Column(name = "ORDER_ID", nullable = false, precision = 0)
    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "INSERT_DATE", nullable = true)
    public Timestamp getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Timestamp insertDate) {
        this.insertDate = insertDate;
    }

    @Basic
    @Column(name = "LAST_UPDATE_DATE", nullable = true)
    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Basic
    @Column(name = "OPTLOCK", nullable = true, precision = 0)
    public Long getOptlock() {
        return optlock;
    }

    public void setOptlock(Long optlock) {
        this.optlock = optlock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Orders orders = (Orders) o;

        if (orderId != orders.orderId) return false;
        if (insertDate != null ? !insertDate.equals(orders.insertDate) : orders.insertDate != null) return false;
        if (lastUpdateDate != null ? !lastUpdateDate.equals(orders.lastUpdateDate) : orders.lastUpdateDate != null)
            return false;
        if (optlock != null ? !optlock.equals(orders.optlock) : orders.optlock != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (orderId ^ (orderId >>> 32));
        result = 31 * result + (insertDate != null ? insertDate.hashCode() : 0);
        result = 31 * result + (lastUpdateDate != null ? lastUpdateDate.hashCode() : 0);
        result = 31 * result + (optlock != null ? optlock.hashCode() : 0);
        return result;
    }
}
