package com.example.springbootwildfly.helloWorld.order.detail;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "ORDER_DETAIL", schema = "CDGINF", catalog = "")
public class OrderDetail {
    private long orderDetailId;
    private Long price;
    private Long vat;
    private Timestamp insertDate;
    private Timestamp lastUpdateDate;
    private Long optlock;

    @Id
    @Column(name = "ORDER_DETAIL_ID", nullable = false, precision = 0)
    public long getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(long orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    @Basic
    @Column(name = "PRICE", nullable = true, precision = 2)
    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    @Basic
    @Column(name = "VAT", nullable = true, precision = 2)
    public Long getVat() {
        return vat;
    }

    public void setVat(Long vat) {
        this.vat = vat;
    }

    @Basic
    @Column(name = "INSERT_DATE", nullable = true)
    public Timestamp getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Timestamp insertDate) {
        this.insertDate = insertDate;
    }

    @Basic
    @Column(name = "LAST_UPDATE_DATE", nullable = true)
    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Basic
    @Column(name = "OPTLOCK", nullable = true, precision = 0)
    public Long getOptlock() {
        return optlock;
    }

    public void setOptlock(Long optlock) {
        this.optlock = optlock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderDetail that = (OrderDetail) o;

        if (orderDetailId != that.orderDetailId) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (vat != null ? !vat.equals(that.vat) : that.vat != null) return false;
        if (insertDate != null ? !insertDate.equals(that.insertDate) : that.insertDate != null) return false;
        if (lastUpdateDate != null ? !lastUpdateDate.equals(that.lastUpdateDate) : that.lastUpdateDate != null)
            return false;
        if (optlock != null ? !optlock.equals(that.optlock) : that.optlock != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (orderDetailId ^ (orderDetailId >>> 32));
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (vat != null ? vat.hashCode() : 0);
        result = 31 * result + (insertDate != null ? insertDate.hashCode() : 0);
        result = 31 * result + (lastUpdateDate != null ? lastUpdateDate.hashCode() : 0);
        result = 31 * result + (optlock != null ? optlock.hashCode() : 0);
        return result;
    }
}
