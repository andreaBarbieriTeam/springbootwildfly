package com.example.springbootwildfly.helloWorld.document;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Document {
    private long docId;
    private String docType;
    private String status;
    private Long tot;
    private Long totVat;
    private Timestamp insertDate;
    private Timestamp lastUpdateDate;
    private Long optlock;

    @Id
    @Column(name = "DOC_ID", nullable = false, precision = 0)
    public long getDocId() {
        return docId;
    }

    public void setDocId(long docId) {
        this.docId = docId;
    }

    @Basic
    @Column(name = "DOC_TYPE", nullable = true, length = 1)
    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, length = 1)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "TOT", nullable = true, precision = 2)
    public Long getTot() {
        return tot;
    }

    public void setTot(Long tot) {
        this.tot = tot;
    }

    @Basic
    @Column(name = "TOT_VAT", nullable = true, precision = 2)
    public Long getTotVat() {
        return totVat;
    }

    public void setTotVat(Long totVat) {
        this.totVat = totVat;
    }

    @Basic
    @Column(name = "INSERT_DATE", nullable = true)
    public Timestamp getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Timestamp insertDate) {
        this.insertDate = insertDate;
    }

    @Basic
    @Column(name = "LAST_UPDATE_DATE", nullable = true)
    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Basic
    @Column(name = "OPTLOCK", nullable = true, precision = 0)
    public Long getOptlock() {
        return optlock;
    }

    public void setOptlock(Long optlock) {
        this.optlock = optlock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Document document = (Document) o;

        if (docId != document.docId) return false;
        if (docType != null ? !docType.equals(document.docType) : document.docType != null) return false;
        if (status != null ? !status.equals(document.status) : document.status != null) return false;
        if (tot != null ? !tot.equals(document.tot) : document.tot != null) return false;
        if (totVat != null ? !totVat.equals(document.totVat) : document.totVat != null) return false;
        if (insertDate != null ? !insertDate.equals(document.insertDate) : document.insertDate != null) return false;
        if (lastUpdateDate != null ? !lastUpdateDate.equals(document.lastUpdateDate) : document.lastUpdateDate != null)
            return false;
        if (optlock != null ? !optlock.equals(document.optlock) : document.optlock != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (docId ^ (docId >>> 32));
        result = 31 * result + (docType != null ? docType.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (tot != null ? tot.hashCode() : 0);
        result = 31 * result + (totVat != null ? totVat.hashCode() : 0);
        result = 31 * result + (insertDate != null ? insertDate.hashCode() : 0);
        result = 31 * result + (lastUpdateDate != null ? lastUpdateDate.hashCode() : 0);
        result = 31 * result + (optlock != null ? optlock.hashCode() : 0);
        return result;
    }
}
