
package com.example.springbootwildfly.helloWorld.webservices.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codeRequest" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="descriptionRequest" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="dateElementRequest" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="dateTimeElementRequest" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="timeElementRequest" type="{http://www.w3.org/2001/XMLSchema}time"/&gt;
 *         &lt;element name="floatElementRequest" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="doubleElementRequest" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codeRequest",
    "descriptionRequest",
    "dateElementRequest",
    "dateTimeElementRequest",
    "timeElementRequest",
    "floatElementRequest",
    "doubleElementRequest"
})
@XmlRootElement(name = "HelloWorld")
public class HelloWorld {

    protected int codeRequest;
    @XmlElement(required = true)
    protected String descriptionRequest;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateElementRequest;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTimeElementRequest;
    @XmlElement(required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar timeElementRequest;
    protected float floatElementRequest;
    protected double doubleElementRequest;

    /**
     * Recupera il valore della proprietà codeRequest.
     * 
     */
    public int getCodeRequest() {
        return codeRequest;
    }

    /**
     * Imposta il valore della proprietà codeRequest.
     * 
     */
    public void setCodeRequest(int value) {
        this.codeRequest = value;
    }

    /**
     * Recupera il valore della proprietà descriptionRequest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescriptionRequest() {
        return descriptionRequest;
    }

    /**
     * Imposta il valore della proprietà descriptionRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescriptionRequest(String value) {
        this.descriptionRequest = value;
    }

    /**
     * Recupera il valore della proprietà dateElementRequest.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateElementRequest() {
        return dateElementRequest;
    }

    /**
     * Imposta il valore della proprietà dateElementRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateElementRequest(XMLGregorianCalendar value) {
        this.dateElementRequest = value;
    }

    /**
     * Recupera il valore della proprietà dateTimeElementRequest.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTimeElementRequest() {
        return dateTimeElementRequest;
    }

    /**
     * Imposta il valore della proprietà dateTimeElementRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTimeElementRequest(XMLGregorianCalendar value) {
        this.dateTimeElementRequest = value;
    }

    /**
     * Recupera il valore della proprietà timeElementRequest.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeElementRequest() {
        return timeElementRequest;
    }

    /**
     * Imposta il valore della proprietà timeElementRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeElementRequest(XMLGregorianCalendar value) {
        this.timeElementRequest = value;
    }

    /**
     * Recupera il valore della proprietà floatElementRequest.
     * 
     */
    public float getFloatElementRequest() {
        return floatElementRequest;
    }

    /**
     * Imposta il valore della proprietà floatElementRequest.
     * 
     */
    public void setFloatElementRequest(float value) {
        this.floatElementRequest = value;
    }

    /**
     * Recupera il valore della proprietà doubleElementRequest.
     * 
     */
    public double getDoubleElementRequest() {
        return doubleElementRequest;
    }

    /**
     * Imposta il valore della proprietà doubleElementRequest.
     * 
     */
    public void setDoubleElementRequest(double value) {
        this.doubleElementRequest = value;
    }

}
