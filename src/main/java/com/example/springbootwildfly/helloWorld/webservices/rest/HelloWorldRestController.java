package com.example.springbootwildfly.helloWorld.webservices.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloWorldRestController {

    @RequestMapping(value="/helloWorld", method = RequestMethod.GET)
    public String index() {
        return "Hello World!";
    }
}
