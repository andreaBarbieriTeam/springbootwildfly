
package com.example.springbootwildfly.helloWorld.webservices.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codeResponse" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="descriptionResponse" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="dateElementResponse" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="dateTimeElementResponse" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="timeElementResponse" type="{http://www.w3.org/2001/XMLSchema}time"/&gt;
 *         &lt;element name="doubleElementResponse" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codeResponse",
    "descriptionResponse",
    "dateElementResponse",
    "dateTimeElementResponse",
    "timeElementResponse",
    "doubleElementResponse"
})
@XmlRootElement(name = "HelloWorldResponse")
public class HelloWorldResponse {

    protected int codeResponse;
    @XmlElement(required = true)
    protected String descriptionResponse;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateElementResponse;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTimeElementResponse;
    @XmlElement(required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar timeElementResponse;
    protected double doubleElementResponse;

    /**
     * Recupera il valore della proprietà codeResponse.
     * 
     */
    public int getCodeResponse() {
        return codeResponse;
    }

    /**
     * Imposta il valore della proprietà codeResponse.
     * 
     */
    public void setCodeResponse(int value) {
        this.codeResponse = value;
    }

    /**
     * Recupera il valore della proprietà descriptionResponse.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescriptionResponse() {
        return descriptionResponse;
    }

    /**
     * Imposta il valore della proprietà descriptionResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescriptionResponse(String value) {
        this.descriptionResponse = value;
    }

    /**
     * Recupera il valore della proprietà dateElementResponse.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateElementResponse() {
        return dateElementResponse;
    }

    /**
     * Imposta il valore della proprietà dateElementResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateElementResponse(XMLGregorianCalendar value) {
        this.dateElementResponse = value;
    }

    /**
     * Recupera il valore della proprietà dateTimeElementResponse.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTimeElementResponse() {
        return dateTimeElementResponse;
    }

    /**
     * Imposta il valore della proprietà dateTimeElementResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTimeElementResponse(XMLGregorianCalendar value) {
        this.dateTimeElementResponse = value;
    }

    /**
     * Recupera il valore della proprietà timeElementResponse.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeElementResponse() {
        return timeElementResponse;
    }

    /**
     * Imposta il valore della proprietà timeElementResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeElementResponse(XMLGregorianCalendar value) {
        this.timeElementResponse = value;
    }

    /**
     * Recupera il valore della proprietà doubleElementResponse.
     * 
     */
    public double getDoubleElementResponse() {
        return doubleElementResponse;
    }

    /**
     * Imposta il valore della proprietà doubleElementResponse.
     * 
     */
    public void setDoubleElementResponse(double value) {
        this.doubleElementResponse = value;
    }

}
