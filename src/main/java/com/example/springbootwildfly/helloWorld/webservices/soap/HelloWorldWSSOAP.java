package com.example.springbootwildfly.helloWorld.webservices.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.1
 * 2018-01-10T15:13:12.179+01:00
 * Generated source version: 3.2.1
 * 
 */
@WebService(targetNamespace = "http://localhost:7001/springbootwildfly/helloWorldWSSOAP/", name = "helloWorldWSSOAP")
@XmlSeeAlso({ObjectFactory.class})
public interface HelloWorldWSSOAP {

    @WebMethod(operationName = "HelloWorld", action = "http://localhost:7001/springbootwildfly/helloWorldWSSOAP/HelloWorld")
    @RequestWrapper(localName = "HelloWorld", targetNamespace = "http://localhost:7001/springbootwildfly/helloWorldWSSOAP/", className = "HelloWorld")
    @ResponseWrapper(localName = "HelloWorldResponse", targetNamespace = "http://localhost:7001/springbootwildfly/helloWorldWSSOAP/", className = "HelloWorldResponse")
    public void helloWorld(
        @WebParam(name = "codeRequest", targetNamespace = "")
        int codeRequest,
        @WebParam(name = "descriptionRequest", targetNamespace = "")
        java.lang.String descriptionRequest,
        @WebParam(name = "dateElementRequest", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar dateElementRequest,
        @WebParam(name = "dateTimeElementRequest", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar dateTimeElementRequest,
        @WebParam(name = "timeElementRequest", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar timeElementRequest,
        @WebParam(name = "floatElementRequest", targetNamespace = "")
        float floatElementRequest,
        @WebParam(name = "doubleElementRequest", targetNamespace = "")
        double doubleElementRequest,
        @WebParam(mode = WebParam.Mode.OUT, name = "codeResponse", targetNamespace = "")
        javax.xml.ws.Holder<java.lang.Integer> codeResponse,
        @WebParam(mode = WebParam.Mode.OUT, name = "descriptionResponse", targetNamespace = "")
        javax.xml.ws.Holder<java.lang.String> descriptionResponse,
        @WebParam(mode = WebParam.Mode.OUT, name = "dateElementResponse", targetNamespace = "")
        javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar> dateElementResponse,
        @WebParam(mode = WebParam.Mode.OUT, name = "dateTimeElementResponse", targetNamespace = "")
        javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar> dateTimeElementResponse,
        @WebParam(mode = WebParam.Mode.OUT, name = "timeElementResponse", targetNamespace = "")
        javax.xml.ws.Holder<javax.xml.datatype.XMLGregorianCalendar> timeElementResponse,
        @WebParam(mode = WebParam.Mode.OUT, name = "doubleElementResponse", targetNamespace = "")
        javax.xml.ws.Holder<java.lang.Double> doubleElementResponse
    ) throws HelloWorldFault_Exception;
}
